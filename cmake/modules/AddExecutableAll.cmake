###
# Adds an executable with all flags and creates symbolic file links
# to grids and input files
# ATTENTION: symbolic links are not supported by all file systems, e. g.
# it will not work on Windows.
#
# Arguments:
# - dumux_test_executable:        name of the executable
# - dumux_test_executable_source: name of the source file
###
macro(add_executable_all dumux_test_executable dumux_test_executable_source)
  # if present, add file/folder link
  if(IS_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/grids)
    dune_symlink_to_source_files(FILES grids)
  endif()
  add_input_file_links()
  add_executable(${dumux_test_executable} EXCLUDE_FROM_ALL ${dumux_test_executable_source})
endmacro()
